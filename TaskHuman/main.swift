//  main.swift
//  TaskHuman
//
//  Created by Private on 12/16/17.
//  Copyright © 2017 Private. All rights reserved.
//

import Foundation

    let Homo = Human(name:"Homo sapien",height: 1.5, weight: 100, gender: "Male")
    let Contador = Cyclist(name:"Alberto Contador",height: 1.7, weight: 75, gender:"Male")
    let Bolt = Runner(name:"Usain Bolt",height: 1.9, weight: 70, gender:"Male")
    let Beard = Swimmer(name:"Amanda Beard",height: 1.8, weight: 65, gender:"Female")
    let White = Snowboarder(name:"Shaun White", height: 1.8, weight: 70, gender: "Male", tool: "Snowboard", color: "Red", destination: "Austria")
    
    let Eagle = Bird(name: "Eagle", length: 0.7 ,numberOfLimbs: 2)
    let Caiman = Lizard(name: "Caiman", length: 5, numberOfLimbs: 4)
    let Opo = Dolphin(name: "Opo", length: 2.5 ,numberOfLimbs: 0)

let HumanArray = [Homo,Contador,Bolt,Beard,White]
let AnimalArray = [Eagle,Caiman,Opo]

for value in HumanArray.reversed(){
    print (value.display())
}

var HumansAndAnimals: [Creator] = [Homo,Opo,Bolt,Beard,Eagle,Caiman,Contador,White]

for value in HumansAndAnimals {
    if let h = value as? Human {
        print("\nHuman")
        print (h.display())
    } else if let a = value as? Animal {
        print ("\nAnimal")
        print (a.display())
    }
}

for(index, item) in HumanArray.enumerated() {
    print(item.name)
    if index < AnimalArray.count {
        print(AnimalArray[index].name)
    }
}

var humansArray: [Creator] = []
var animalsArray: [Creator] = []
var sortedHumanArray: [Creator] = []
var sortedAnimalsArray: [Creator] = []

for (_,value) in HumansAndAnimals.enumerated() {
    if let h = value as? Human {
        humansArray.append(h)
        sortedHumanArray = humansArray.sorted(by: { (firstCreature: Creator, nextCreature: Creator) -> Bool in
            return firstCreature.name < nextCreature.name
        } )
    }
    else if let animal = value as? Animal {
        animalsArray.append(animal)
        sortedAnimalsArray = animalsArray.sorted(by: { (firstCreature: Creator, nextCreature: Creator) -> Bool in
            return firstCreature.name < nextCreature.name
        })
    } else {
        break
    }
}

for human in sortedHumanArray {
    print("This is \(human.name)")
}

for animal in sortedAnimalsArray {
    print("This is \(animal.name)")
}

var sortedArray:[Creator] = []

sortedArray = sortedHumanArray + sortedAnimalsArray

for creature in sortedArray {
    print(creature.name)
}

