//
//  Human.swift
//  TaskHuman
//
//  Created by Private on 12/16/17.
//  Copyright © 2017 Private. All rights reserved.
//

import Foundation

class Human: Creator {
    var height: Double
    var weight: Int
    var gender: String
    
    init(name: String, height: Double, weight: Int, gender: String) {
        self.height = height
        self.weight = weight
        self.gender = gender
        super.init(name: name)
    }
    
    func move(){
        print ("I can move now!")
    }
    
    func display(){
        print("\nName: \(name)\nHeight: \(height)\nWeight: \(weight)\nGender: \(gender)")
        move()
    }
}
