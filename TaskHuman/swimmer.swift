//
//  swimmer.swift
//  TaskHuman
//
//  Created by Private on 12/16/17.
//  Copyright © 2017 Private. All rights reserved.
//

import Foundation

class Swimmer: Human {
    
    override init(name: String, height: Double, weight: Int, gender: String) {
        super.init(name: name, height: height, weight:weight, gender: gender)
    }
    
    func specialMove(){
        print("+ I can swimm now!")
    }
    
    override func move(){
        super.move()
        specialMove()
    }
}
