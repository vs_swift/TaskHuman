//
//  Creator.swift
//  TaskHuman
//
//  Created by Private on 12/24/17.
//  Copyright © 2017 Private. All rights reserved.
//

import Foundation

class Creator {
    var name: String
    
    init(name: String) {
        self.name = name
    }
}
