//
//  Bird.swift
//  TaskHuman
//
//  Created by Private on 12/16/17.
//  Copyright © 2017 Private. All rights reserved.
//

import Foundation

class Bird: Animal {
    
    override init(name: String, length: Double,numberOfLimbs: Int) {
        super.init(name: name, length: length,numberOfLimbs: numberOfLimbs)
    }
    
    func specialMove(){
        print("+ I can Fly!")
    }
    override func move(){
        super.move()
        specialMove()
    }
}

