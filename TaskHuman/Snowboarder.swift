//
//  Snowboarder.swift
//  TaskHuman
//
//  Created by Private on 12/16/17.
//  Copyright © 2017 Private. All rights reserved.
//

import Foundation

class Snowboarder: Human {
    
    let tool: String
    let color: String
    let destination: String
    
    init(name: String, height: Double, weight: Int, gender: String, tool: String,color: String,  destination: String) {
        self.tool = tool
        self.color = color
        self.destination = destination
        
        super.init(name: name, height: height, weight:weight, gender: gender)
    }
    func specialMove(){
        print("+ I can do snowboarding now!")
    }
    override func move(){
        super.move()
        specialMove()
    }
    override func display(){
        print("\nName: \(name)\nHeight: \(height)\nWeight: \(weight)\nGender: \(gender)\nTool: \(tool)\nColor: \(color)\nDestination: \(destination)")
        move()
    }
}
