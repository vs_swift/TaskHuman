//
//  lizard.swift
//  TaskHuman
//
//  Created by Private on 12/16/17.
//  Copyright © 2017 Private. All rights reserved.
//

import Foundation

class Lizard: Animal {
    
    override init(name: String, length: Double,numberOfLimbs: Int) {
        super.init(name: name, length: length,numberOfLimbs: numberOfLimbs)
    }
    
    func specialMove(){
        print("+ I can crawl")
    }
    override func move(){
        super.move()
        specialMove()
    }
}
