//
//  Animal.swift
//  TaskHuman
//
//  Created by Private on 12/16/17.
//  Copyright © 2017 Private. All rights reserved.
//

import Foundation

class Animal: Creator {
    var length: Double
    var numberOfLimbs: Int
    
    init(name: String, length: Double,numberOfLimbs: Int) {
        self.length = length
        self.numberOfLimbs = numberOfLimbs
        super.init(name: name)
    }
    
    func move(){
        print("I can get somewhere")
    }
    
    func display(){
        print("\nTitle: \(name)\nLength: \(length)\nNumber Of Limbs: \(numberOfLimbs)")
        move()
    }
}
